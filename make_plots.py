exec(open('histoscripts/buildBinnedHistos.py').read())
exec(open('histoscripts/readHistos.py').read())

from matplotlib.colors import LogNorm

def FBH():

	fillBinnedHistosFromJetData(inp, [DeltaPhi_in_BINS_UFO_005, DeltaEta_in_BINS_UFO_005, mR_in_BINS_UFO_005, eR_in_BINS_UFO_005 ], outName= 'out.h5', append=True, filter= filterMatched, addVar =["ufojet_truthMatch", "pflowjet_truthMatch"])
	
	#fillBinnedHistosFromJetData(inp, [mR_in_BINS_UFO, mR_in_BINS_UFO_m0], outName= 'out.h5', append=True, filter= filterMatched, addVar =["ufojet_rmass","ufojet_E","ufojet_truthMatch","pflowjet_truthMatch"])
	
	#fillBinnedHistosFromJetData(inp, [DeltaPhi_in_BINS_UFO, DeltaEta_in_BINS_UFO, DeltaPhi_in_BINS_PFlow, DeltaEta_in_BINS_PFlow, eR_in_BINS_UFO, mR_in_BINS_UFO,eR_in_BINS_PFlow, mR_in_BINS_PFlow ], outName= 'out.h5', append=True, filter= filterMatched, addVar =["ufojet_truthMatch", "pflowjet_truthMatch"])


def reponse(xaxis, xlim, fileName):	

	gs.readBAFromFile(fileName, styleFunc=atlasStyle)
	      
	#gs.DeltaEta_in_pTeta_PFlow.loadContent()
	#gs.DeltaEta_in_pTeta_UFO.loadContent()

	#gs.DeltaEta_in_pTeta_PFlow.applyFunc( normHistos )
	#gs.DeltaEta_in_pTeta_UFO.applyFunc( normHistos ) 

	#gs.drawManyHistos([gs.DeltaEta_in_pTeta_UFO, gs.DeltaEta_in_pTeta_PFlow],  descPos=dict(x0=0.6,fontsize='large'), outname='DeltaEta.pdf', xlabel='Jet DeltaEta')
		
	gs.buildMany1DGraphs('DeltaEta_in_pTeta', 'UFO,PFlow,UFO_005', iqrFromBHnotrel, vsDim=xaxis, graphStyle= {'marker':'o'})
	gs.buildMany1DGraphs('DeltaEta_in_pTeta', 'UFO,PFlow,UFO_005', modeFromBH, vsDim=xaxis, graphStyle= {'marker':'o'})	
			
	gs.compareGraphs('DeltaEta_in_pTeta', 'UFO,PFlow,UFO_005', xaxis, iqrFromBHnotrel, addBinN=False, ylabel= 'Delta eta IQR',  ylim=(0, 0.06), nplots=1, xlim=xlim)
	gs.compareGraphs('DeltaEta_in_pTeta', 'UFO,PFlow,UFO_005', xaxis, modeFromBH, addBinN=False, ylabel= 'Delta eta mode', ylim=(-0.05,0.05), nplots=1, xlim=xlim)
