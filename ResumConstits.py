import numba
import awkward as ak
import numpy as np

@numba.njit
def buildJetConstitIndexFullNoSel(N, jet_constitIndex, evt_constitpt):
    #N = len(jet_constitIndex.layout.content.content)
    out = np.empty(N, dtype=np.int64)

    evtcstOffset = 0
    ci=0
    for ev, jetconstI in enumerate(jet_constitIndex):
        for constI in jetconstI:
            for i in constI:
                out[ci] = evtcstOffset+i
                ci+=1
        evtcstOffset+= len(evt_constitpt[ev])
    return out


@numba.njit
def buildJetConstitIndexFullSel(selectedjetsI,  jet_constitIndex_jet, evt_njets, evt_constitoffset):
    """returns the fully flat indices of constituent belonging to jets indexed by selectedJetsI
      - selectedJetsI : fully flat jet indices
      - jet_constitIndex_jet : the awkward array containing the indicies of constits in jets ( == flatten(ufojet_ConstitIndex))
      - evt_njets : number of jets (1 entry per event)
      - evt_constitoffset : offsets of full constituents list (1 entry per event) == [0,]+evt_nconstit.cumsum()
    """
    N : np.int64 = 0

    
    # count the num of constit in the selected jets
    for jetInd in selectedjetsI:
        N+= len(jet_constitIndex_jet[jetInd])
    
    out = np.empty(N, dtype=np.int64)

    # expand the offsets of the full constituents from event position to each jet position
    jet_constitOffset = np.repeat(evt_constitoffset, evt_njets)

    evtcstOffset = 0
    ci : jet_constitOffset.dtype  =0

    # fill the output
    for jetInd in selectedjetsI:
        evtcstOffset = jet_constitOffset[jetInd]
        jetConstI = jet_constitIndex_jet[jetInd]
        for i in jetConstI:
            out[ci] = evtcstOffset+i
            ci+=1
    return out

def flattenConstitInd(selectedjetsI, constitVar='ufoconstit_E'):
    """
    WARNING : same as in buildBinnedHistos.py ...
    """
    from Variables import npViewFromAwkArray, knownVars
    prefix = 'ufo' if 'ufo' in constitVar else 'pflow'
    jet_constitIndex = knownVars[prefix+'jet_ConstitIndex'].awkarray
    evt_constitX = knownVars[constitVar].awkarray
    jet_nconstit = npViewFromAwkArray(ak.num(jet_constitIndex,axis=2), np.int64)
    if isinstance(selectedjetsI, slice):
        N = selectedjetsI.stop
        #print(jet_constitIndex, evt_constitX)        
        constitIndices= buildJetConstitIndexFullNoSel(N, jet_constitIndex, evt_constitX)        
    else:
        evt_njets = npViewFromAwkArray(ak.num(jet_constitIndex), np.int64)
        # Using a jet selection.
        # in this case : use a flatten version of constitIndices 
        jet_constitIndex = ak.flatten(jet_constitIndex)
        evt_constitOffset = np.asanyarray(evt_constitX.layout.offsets)
        #print(constitVar, "aaaa ",evt_constitOffset.size, evt_njets.size)
        constitIndices= buildJetConstitIndexFullSel(selectedjetsI, jet_constitIndex, evt_njets, evt_constitOffset[:-1])
    return constitIndices, jet_nconstit

@numba.generated_jit(nopython=True)
#numba.njit
def addat(target, index, tobeadded):
    """Equivalent of np.add.at(target, index, tobeadded) but much faster. """
    def addat_scalar(target, index, tobeadded):
        for i in range( index.size):        
            target[index[i]] += tobeadded
    def addat_vector(target, index, tobeadded):
        for i in range( index.size):        
            target[index[i]] += tobeadded[i]
    if isinstance(tobeadded, numba.types.Number) :
        #if isinstance(tobeadded, (int, float, complex, bool)) or tobeadded.ndim==0:        
        return addat_scalar
    else:
        return addat_vector

def segment_sum(data, segment_ids,out=None, outN=None):
    """same as tf.segment_sum but for numpy """
    if out is None:
        outN = outN or  (np.max(segment_ids)+1)
        if  isinstance(data, (int, float, complex, bool)) :
            out = np.zeros( outN)
        else:
            data = np.asarray(data)
            out = np.zeros( (outN,)+ data.shape[1:] , dtype=data.dtype)            
    else:
        out[:]=0
    addat(out, segment_ids, data)
    return out


@numba.njit
def eetaphiToEpxpypz(e,eta,phi):
    
    p4 = np.empty( (e.shape[0],4), dtype=e.dtype )
    pt = e/np.cosh(eta)
    
    p4[:,1] = pt*np.cos(phi)
    p4[:,2] = pt*np.sin(phi)
    p4[:,3] = pt*np.sinh(eta)
    p4[:,0] = e
    return p4

def mass(p4):
    p42= p4*p4
    return np.sqrt(p42[:,0] - p42[:,1:].sum(axis=1) )

def mass2(p4):
    p42= p4*p4
    return p42[:,0] - p42[:,1:].sum(axis=1) 

def resumJetConstit(jetInd, cst='ufo'):
    from Variables import knownVars
    
    cstInFlat, jet_nconstit = flattenConstitInd(jetInd, cst+'constit_E')
    jet_nconstit = jet_nconstit[jetInd]
    totNjet = jet_nconstit.size
    parentJetI = np.repeat(np.arange(totNjet), jet_nconstit)
    E=knownVars[f'{cst}constit_E'].rawarray[cstInFlat]
    eta=knownVars[f'{cst}constit_eta'].rawarray[cstInFlat]
    phi=knownVars[f'{cst}constit_phi'].rawarray[cstInFlat]

    p4 = eetaphiToEpxpypz(E,eta,phi)

    #return
    jetP4 = segment_sum( p4, parentJetI , outN=totNjet)
    return jetP4

