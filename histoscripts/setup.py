import uproot
import awkward as ak
from Variables import Variable, knownVars
from Inputs import Inputs
from ConfigUtils import ConfigDict
from HistoUtils import Histo1D


config = ConfigDict(
    inputDir = '',
    filePattern = "*.root",
    treename  = '',
    
    varList = ["ufojet_r_rm","ufojet_rmass", "ufojet_combinedFrac", "ufojet_neutralFrac", "pflowjet_neutralFrac", "ufojet_chargedFrac", "pflowjet_chargedFrac", "truejet_rapidity", "ufojet_rapidity", "pflowjet_rapidity", "ufojet_deltaEta", "pflowjet_deltaEta", "ufojet_deltaPhi", "pflowjet_deltaPhi", "ufojet_r_e", "pflowjet_r_e", "ufojet_r_m", "pflowjet_r_m", "pflowjet_truthMatch", "ufojet_truthMatch", "pflowjet_isolated", "ufojet_isolated", "truejet_phi", "pflowjet_phi", "ufojet_phi", "truejet_pt", "ufojet_pt", "pflowjet_pt", "truejet_eta", "pflowjet_eta", "ufojet_eta", "truejet_mass", "pflowjet_mass", "ufojet_mass", "truejet_E", "pflowjet_E", "ufojet_E"], 
  

    step_size = '1200 MB', # the maximum size of arrays to load from 1 file in memory
    )


inp = Inputs(config)





