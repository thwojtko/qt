"""
This script includes code to fill massive sets of histograms out of in-file data

Typically it is used to fill response histograms 
  - in each bins of a pre-defined phase space
  - from all input file 

It first defines 
 - bin phases spaces (using HistoAnalysis.BinnedArrays.BinnedPhaseSpace)
 - container of histos corresponding to the phase space (using HistoAnalysis.BinnedArrays.BinnedHistos)
 - function to loop over inputs and fill the histograms.

The call has the form :

fillBinnedHistosFromJetData(input, [listof BinnedHistos containers] , beginFileTask=someFunction, outName="outfile")

where someFunction(input, filei) is expected to perform tasks after filei has been loaded in memory.

"""

from HistoAnalysis.BinnedArrays import *

import matplotlib.pyplot as plt
plt.ion()
import os
import os.path


def symmetrize(l):
    """ given [0, a, b, c], returns [-c, -b, -a, 0, a, b, c]"""
    newL =[-i for i in l[1:] ] #assumee l starts with 0.
    newL.reverse()
    return newL+l

def seq(start, stop, step):
    return list(np.arange(start, stop+0.99*step, step))


##**************************************************
##**************************************************
# Define the phase spaces (as BinnedPhaseSpace) in which we will fill histograms.
#  We add a 'coords' member containing the names of the variables (in Variables.py) representing the dimensions of the phasespace
GeV = 1.
pTEtaBPS = BinnedPhaseSpace("pTEtaBPS",
                           AxisSpec("pT", title="pT [GeV]",edges=np.array([10,70, 100,150, 200,4000])*GeV ),
                           AxisSpec("eta", title="$\eta$", edges=np.array([-2.8, -2.5, 2.5, 2.8]), isGeV=False),
                           )
# add information : the name of the Variable objects representing the coordinates
pTEtaBPS.coords = ('truejet_pt', 'truejet_eta')


#choice of the BPS

BPS = pTEtaBPS

##**************************************************
##**************************************************
# Define the histogram containers (BinnedHistos)

def generateBinnedHistos( name, bps, valueName, hbins ):
    """shortcut function to define BinnedHistos in 1 line, including the name of the variable to be histogrammed (valueName) """
    bpsTag = bps.axisTag()
    bh = BinnedHistos( name.replace("BPS", bpsTag), bps, hbins , initArrays=False)
    # add information : the name of the Variable objects representing the coordinates & the histogrammed value
    bh.coordNames = bps.coords
    bh.valueName = valueName
    return bh



DeltaEta_in_BINS_UFO = generateBinnedHistos("DeltaEta_in_BPS_UFO", BPS, 'ufojet_deltaEta' ,(240,(-0.25,0.25),'DeltaEta: DeltaEta') )
DeltaEta_in_BINS_UFO_005 = generateBinnedHistos("DeltaEta_in_BPS_UFO_005", BPS, 'ufojet_deltaEta' ,(240,(-0.25,0.25),'DeltaEta: DeltaEta') )
DeltaEta_in_BINS_PFlow = generateBinnedHistos("DeltaEta_in_BPS_PFlow", BPS, 'pflowjet_deltaEta' ,(240,(-0.25,0.25),'DeltaEta: DeltaEta') )


DeltaPhi_in_BINS_UFO = generateBinnedHistos("DeltaPhi_in_BPS_UFO", BPS, 'ufojet_deltaPhi' ,(240,(-0.15,0.15),'DeltaPhi: DeltaPhi') )
DeltaPhi_in_BINS_UFO_005 = generateBinnedHistos("DeltaPhi_in_BPS_UFO_005", BPS, 'ufojet_deltaPhi' ,(240,(-0.15,0.15),'DeltaPhi: DeltaPhi') )
DeltaPhi_in_BINS_PFlow = generateBinnedHistos("DeltaPhi_in_BPS_PFlow", BPS, 'pflowjet_deltaPhi' ,(240,(-0.15,0.15),'DeltaPhi: DeltaPhi') )


eR_in_BINS_UFO = generateBinnedHistos("eR_in_BPS_UFO", BPS, "ufojet_r_e", (240,(0,2),'EnergieResponse: EnergyResponse'))
eR_in_BINS_UFO_005 = generateBinnedHistos("eR_in_BPS_UFO_005", BPS, "ufojet_r_e", (240,(0,2),'EnergieResponse: EnergyResponse'))
eR_in_BINS_PFlow = generateBinnedHistos("eR_in_BPS_PFlow", BPS, "pflowjet_r_e", (240,(0,2),'EnergieResponse: EnergyResponse'))


mR_in_BINS_UFO = generateBinnedHistos("mR_in_BPS_UFO",BPS, "ufojet_r_m", (240,(0,2),'MassResponse: Mass:Response'))
mR_in_BINS_UFO_005 = generateBinnedHistos("mR_in_BPS_UFO_005", BPS, "ufojet_r_m", (240,(0,2),'MassResponse: Mass:Response'))
mR_in_BINS_PFlow = generateBinnedHistos("mR_in_BPS_PFlow", BPS, "pflowjet_r_m", (240,(0,2),'MassResponse: Mass:Response'))


mR_in_BINS_UFO_m0 = generateBinnedHistos("mR_in_BPS_UFO_m0",BPS, "ufojet_r_rm", (240,(0,2),'MassResponse: Mass:Response'))


def filterMatched(N):
       
       ufomatched = knownVars.ufojet_truthMatch.rawarray[:N]
       pflowmatched = knownVars.pflowjet_truthMatch.rawarray[:N]

       return np.flatnonzero(np.logical_and(ufomatched==1 ,pflowmatched==1 ))


def normHistos(coord,container):
     container.histoAt( coord).normalize()



def detectConstitVar(cont):
    vnames = cont.valueName if isinstance(cont.valueName, tuple) else (cont.valueName,)
    for v in vnames:
        if v.startswith('ufoconstit') or v.startswith('pflowconstit'):
            print("!! Detected Constit var !! ",v)
            return v
    return None



def fillBinnedHistosFromJetData(inputs, bhList, useWeights=False, beginFileTask=None, outName=None, nFile=-1, append=False,filter=None, addVar = []   ):
    """Fill histos in each the BinnedHisto containets in the 'bhList' list. 

    The function will loop on each input file (as defined by inputs.fileList), load the data, call the function beginFileTask and 
    fill the histograms. 

    beginFileTask is optionnal. If given it will be called as 
      beginFileTask(inputs, filei)
    where filei is the index of the file being processed.

    """

    if useWeights:
        print("!!! Weights not yet supported !!! ")
        return

    inputs.sortFiles()

    histDimMax = 2 if any(isinstance(bh,BinnedHistos2) for bh in bhList) else 1
 
    

   
 
    # group by bps to factorize indices calculations
    bhDict = dict()
    allVarNames = set()
    for bh in bhList:
        bh.initArrays()
        bhDict.setdefault( (bh.bps,)+bh.coordNames, []).append(bh)
        bhvars =  bh.valueName if isinstance(bh,BinnedHistos2) else (bh.valueName,) 
        bhvars = bhvars+bh.coordNames
        allVarNames.update( bhvars )
        
    allVarNames.update( sum([knownVars[v].dependencies()  for v in allVarNames ], [] ) )
    # at this point allVarNames contains all jet var needed to fill histos 
                           
    allVarNames.update( addVar ) 

    # detect if we are plotting constituent vars :
    
    constitVar = detectConstitVar(bhList[0])

    print(constitVar)
    allVarNames = list(allVarNames)
    maxDimBPS = max( bhList , key=lambda bh:bh.bps.nDim() ).bps
    inputs.setVarList(allVarNames)
    
    #return allVarNames

    binIndices = None

    totalN= 0
    for i,a in enumerate(inputs.iterate()):        

        print("########################## iteration ",i)


        # call the user function (typically : NN predictions)
        if callable(beginFileTask): beginFileTask(inputs,i)
        v0 = [knownVars[n] for n in allVarNames if knownVars[n].from_file][0]
        N = v0.rawarray.shape[0]


        # filter if needed :
        if filter :
            selectedInd = filter(N)
            N = selectedInd.size

        else:
            selectedInd = slice(0,N)
      


        if binIndices is None or binIndices.shape[-1] <N:
            numJetMax = int(N*1.1) # hope it will be enoug, should be since we sort by num entries...
            print(f"numJetMax {numJetMax} evaluated from , ",v0.name)
            
            # first pass : create a big array suitable to hold all the index coordinates in phase space
            #numJetMax = trainer.inputs.maxNumGraphs() 
            binIndices = maxDimBPS.buildNCoordinates(numJetMax)
            print("binIndices shape =", binIndices.shape)
            #hIndices = np.zeros( (histDimMax,numJetMax) ,dtype=int)
            if constitVar is None:
                hIndices = np.zeros( (histDimMax,numJetMax) ,dtype=int)
            else:
                numJetMax = knownVars[constitVar].rawarray.size
                hIndices = np.zeros( (histDimMax,numJetMax) ,dtype=int)


        
        # All variables are now ready.
        # **********************
        
        totalN +=N
            
        #loop over the BinnedHistos which share the same bps & coordinates 
        for _, bhL in bhDict.items():
            bps = bhL[0].bps 
            print(i,"******************** Fill histos with coodinates ",bps.name, bhL[0].coordNames, N, totalN)

            # For each entry (jet), the histos in this loop share the same phase space coordinates (ex: (pt, eta) of the jet) so we calculate
            # the bin indices of this coordinates only once 
            
            # compute the common index coordinates for these BinnedHistos :
            #  a) prepare the coordinates to be binned ( ex [e, eta, mass])
            #print('ssss' ,[(v,knownVars[v].rawarray.shape[0]) for v in allVarNames ])
            coords = [ knownVars[v].rawarray[selectedInd] for v in bhL[0].coordNames ]
            #return coords, binIndices[:bps.nDim(),:N]
            # b) bin the coordinates into indices (we pass binIndices so it can be re-used internally and does not need to be recreated each time) :
            flatInd, validCoordI = bps.findValidBins(coords, binIndices[:bps.nDim(),:N] , returnFlatI=True)

            print(constitVar)
            if constitVar is not None:
                #selectedConstitInd, jet_nconstit = flattenConstitInd(selectedInd[validCoordI], constitVar)
            
                finalJetSelected = selectedInd[validCoordI]
                selectedConstitInd, jet_nconstit = flattenConstitInd(finalJetSelected, constitVar)
		

                #print( coords[0].shape, ' --> flatInd ',flatInd.shape, '  . ', selectedInd.size, ' validCoordI ',validCoordI.size )
    

                jetToConstitRepeat = jet_nconstit[ finalJetSelected]
                flatInd = np.repeat( flatInd, jetToConstitRepeat )
                #validCoordI = np.repeat( validCoordI, jet_nconstit[selectedInd][validCoordI] )
                validCoordI = np.arange(selectedConstitInd.size)
                # print( coords[0].shape, ' --> flatInd ',flatInd.shape, '  . ', selectedInd.size, ' validCoordI ',validCoordI.size )
                # print( coords[0].shape, ' --> flatInd ',flatInd.shape, '  . ', selectedInd.shape, )
                # print( coords[1][0], ' -->  ',selectedConstitInd[0:jet_nconstit[ selectedInd][validCoordI[0]]], '   ' )
                # print( coords[1][0], ' -->  ',flatInd[0:jet_nconstit[ selectedInd][validCoordI[0]]+1], '   ' )
                # print( coords[1][0], ' -->  ',validCoordI[0:jet_nconstit[ selectedInd][validCoordI[0]]+1], '   ' )
                selectedInd = selectedConstitInd
                N = selectedInd.size



            # now flatInd contains the indices of the bins, and validCoordI is the entries of valid (not out  of bound) coordinates in the orginal coords
            
            # Then fill the histos at the calculated bin indices with their own values 
            #w = w0[validCoordI] if useWeights else 1.
            w = 1.
            w2=w*w
            for bh in bhL:
                print(i,"******************** ---> Fill  BinnedHistos ",bh.name)
                if isinstance(bh,BinnedHistos2):
                    #values = [knownVars[n].rawarray[selectedInd] for n in bh.valueName ]
                    v1, v2 = [knownVars[n] for n in bh.valueName]                    
                    if constitVar:
                        # convert the jet type variable to constit-like variable
                        a1 = np.repeat(v1.rawarray[finalJetSelected], jetToConstitRepeat) if v1.category=='jet' else v1.rawarray[selectedInd]
                        a2 = np.repeat(v2.rawarray[finalJetSelected], jetToConstitRepeat) if v2.category=='jet' else v2.rawarray[selectedInd]
                        values= [a1, a2]
                    else:
                        values = [v1.rawarray[selectedInd] , v2.rawarray[selectedInd] ]


 
                    bh.fillAtIndices( values, validCoordI, flatInd, w, w2,  hIndices=hIndices[:,:N])
                else:
                    values = knownVars[bh.valueName].rawarray[selectedInd]
                    print('--->  ',values[:4])                
                    bh.fillAtIndices( values, validCoordI, flatInd, w, w2, hIndices=hIndices[0,:N])
                    
    if outName:
        print("Saving Binned Histo into ", outName)
        saveBinnedHistosAsH5(bhList, outName,append=append)




def flattenConstitInd(selectedjetsI, constitVar):
    prefix = 'ufo' if 'ufo' in constitVar else 'pflow'
    jet_constitIndex = knownVars[prefix+'jet_ConstitIndex'].awkarray
    evt_constitX = knownVars[constitVar].awkarray
    from Variables import npViewFromAwkArray
    jet_nconstit = npViewFromAwkArray(ak.num(jet_constitIndex,axis=2), np.int64)
    if isinstance(selectedjetsI, slice):
        N = selectedjetsI.stop
        constitIndices= buildJetConstitIndexFullNoSel(N, jet_constitIndexVar, evt_constitX)        
    else:
        # Using a jet selection.
        # in this case : use a flatten version of constitIndices 
        jet_constitIndex = ak.flatten(jet_constitIndex)
        evt_njets = npViewFromAwkArray(ak.num(knownVars['truejet_eta'].awkarray), np.int64)
        evt_constitOffset = np.frombuffer(evt_constitX.layout.offsets, dtype=np.int64)
        #print(constitVar, "aaaa ",evt_constitOffset.size, evt_njets.size)
        constitIndices= buildJetConstitIndexFullSel(selectedjetsI, jet_constitIndex, evt_njets, evt_constitOffset[:-1])
    return constitIndices, jet_nconstit

import numba
@numba.njit
def buildJetConstitIndexFullNoSel(N, jet_constitIndex, evt_constitpt):
    #N = len(jet_constitIndex.layout.content.content)
    out = np.empty(N, dtype=np.int64)

    evtcstOffset = 0
    ci=0
    for ev, jetconstI in enumerate(jet_constitIndex):
        for constI in jetconstI:
            for i in constI:
                out[ci] = evtcstOffset+i
                ci+=1
        evtcstOffset+= len(evt_constitpt[ev])
    return out


@numba.njit
def buildJetConstitIndexFullSel(selectedjetsI,  jet_constitIndex_jet, evt_njets, evt_constitoffset):
    """returns the fully flat indices of constituent belonging to jets indexed by selectedJetsI
      - selectedJetsI : fully flat jet indices
      - jet_constitIndex_jet : the awkward array containing the indicies of constits in jets ( == flatten(ufojet_ConstitIndex))
      - evt_njets : number of jets (1 entry per event)
      - evt_constitoffset : offsets of full constituents list (1 entry per event) == [0,]+evt_nconstit.cumsum()
    """
    N : np.int64 = 0

    
    # count the num of constit in the selected jets
    for jetInd in selectedjetsI:
        N+= len(jet_constitIndex_jet[jetInd])
    
    out = np.empty(N, dtype=np.int64)

    # expand the offsets of the full constituents from event position to each jet position
    jet_constitOffset = np.repeat(evt_constitoffset, evt_njets)

    evtcstOffset = 0
    ci : jet_constitOffset.dtype  =0

    # fill the output
    for jetInd in selectedjetsI:
        evtcstOffset = jet_constitOffset[jetInd]
        jetConstI = jet_constitIndex_jet[jetInd]
        for i in jetConstI:
            out[ci] = evtcstOffset+i
            ci+=1
    return out






def saveBinnedHistosAsH5( bhList, fname,append=False):
    if os.path.exists( fname) and not append : os.remove(fname)        
    for bh in bhList:
        print("... Saving  ", bh.name)        
        bh.saveInH5(fname)

        
def fillHisto(inputs, var, histo):
    if isinstance(var,str):
        var = knownVars[var]
    
    allVarNames =  var.dependencies()
    if var.from_file:
        allVarNames = [var.name]+allVarNames
    inputs.setVarList(allVarNames)
    inputs.concatenate()
    histo.fill(var.rawarray)
    return histo
    
