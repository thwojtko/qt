"""

Simply using an array of variable read from an input file is not enough in realistic situations.
For each variable we also need other information :
 - its name and title (mainly for reference and visualisation/plotting needs)
 - how to transform it if necessary. ex: we may want to construct a ratio out of 2 variables read from input
 
All these info + the data content are conveniently attached to 1 object of type Variable. 
We use one such object for each variable used in the framework.

"""

import numexpr as ne
import numpy as np
import awkward as ak
from ConfigUtils import ConfigDict

def npViewFromAwkArray( awkarray, dtype):
    """Returns a flat np.array which is a view on awkarray (that is: if out=npViewFromAwkArray(in) and out[0]=-1 then in[0]==-1) """
    #ndim = awkarray.ndim
    #if ndim==1:   buff = awkarray.layout
    #elif ndim==2: buff = awkarray.layout.content
    #elif ndim==3: buff = awkarray.layout.content.content
    
    return np.asanyarray(ak.ravel(awkarray))

    #return np.frombuffer(buff, dtype=dtype)



class Variable:
    """Describes one variable,
    """

    from_file = True
    name = ""    
    title=""

    src_name = ""
     
    src_type = np.float32 
     
    rawarray = None
    awkarray = None
 
    hrange = (0, 1)
    
    isint = False # used only for plotting
    
    
    
    def __init__(self, name, src_name ="", **args):
        
        self.name = name
        self.src_name = src_name or name
        args.setdefault('title', name)
        for k,v in args.items():
            setattr(self,k,v)


    def calculate(self,):
        pass
            
    def dependencies(self):
        return []
        
    def loadFromTree(self, t,):

        self.awkarray = t[self.src_name].array()
        
        self.buildRawArray()

    def buildRawArray(self):
        self.rawarray = npViewFromAwkArray(self.awkarray, self.src_type)


    def setRawArray(self,rawarray, awkarray=None):
        self.rawarray = rawarray
        self.awkarray = awkarray
        
 
class JVariable(Variable):
    category="jet"

class EvtVariable(Variable):
    category="event"
    

class CVariable(Variable):
    category="constit"

class ConstitType(Variable):
    category="constit"
    def buildRawArray(self):
        self.rawarray = npViewFromAwkArray(self.awkarray, self.src_type)
        np.right_shift(self.rawarray,13,out=self.rawarray)
    
    
class CalculatedV(Variable):
    """Variable calculated as a generic expresion of other variables. 
    The ctor MUST provide 
    * vars = [ vname1, vname2, .... ] where vnamei are variables in knownVars
    * transfo = " expression of vnamei" a mathematical expression which will be evaluated via numexpr
    """
    from_file = False
    rawarray_data = None

    def calculate(self,):
        vlist = self.vars

        jetVars = []
        for v in vlist:
            var = knownVars[v]
            jetVars.append(var)
            
        ldict = {}       
        for j in jetVars:
            ldict[j.name] = j.rawarray
                
        arraysizes= [ a.size for a in ldict.values() ]        
        minS = min(arraysizes) # this can be lower than the actual array sizes
        # re-use rawarray_data if we can :
        if self.rawarray_data is None or self.rawarray_data.size < minS :
            self.rawarray_data = np.zeros( (minS,) , dtype=np.float32)
        self.rawarray = self.rawarray_data[:minS]

        ldict = dict( (n,a[:minS]) for (n,a) in ldict.items() )

        # evaluate with numexpr :
        if callable(self.transfo):
            self.transfo( out=self.rawarray, **ldict)
        else:
            ne.evaluate( self.transfo , local_dict=ldict, out=self.rawarray)
        

    def dependencies(self):
        vlist = self.vars
        return vlist+sum( ( knownVars[v].dependencies() for v in vlist), [])

class RatioVariable(Variable):
    from_file = False

    def calculate(self,):
        numerV = knownVars[self.numer]
        denomV = knownVars[self.denom]

        if numerV.rawarray is None:
            print(f"WARNING RatioVariable {self.name} : numerator {numerV.name} is None ")
            return
        if denomV.rawarray is None:
            print(f"WARNING RatioVariable {self.name} : denominator {denomV.name} is None ")
            return

        n = min(len(numerV.rawarray),len(denomV.rawarray))
        if self.rawarray is None or len(self.rawarray) < n:            
            self.rawarray = numerV.rawarray[:n]/denomV.rawarray[:n]
        else:
            # try avoiding memory allocation by calculating in-place
            if self.rawarray.base is not None:
                self.rawarray = self.rawarray.base
            self.rawarray[:n] = numerV.rawarray[:n]
            self.rawarray[:n] /= denomV.rawarray[:n]
            self.rawarray = self.rawarray[:n] # just to make sure it seen has having exactly n entries

    def calculate_awk(self):
        numerV = knownVars[self.numer]
        denomV = knownVars[self.denom]
        n = min(len(numerV.rawarray),len(denomV.rawarray))
        self.awkarray = numerV.awkarray[:n]/denomV.awkarray[:n]
        
    def dependencies(self):
        return [self.numer, self.denom]+knownVars[self.numer].dependencies()+knownVars[self.denom].dependencies()


class JetNConstit(JVariable):
    from_file = False
    def calculate(self):
        self.awkarray = ak.num( knownVars[self.constitIndex].awkarray ,axis=2)
        self.rawarray = npViewFromAwkArray( self.awkarray ,dtype=np.int64)

    def dependencies(self):
        return [self.constitIndex]


class OffsetsFromAwkArray(Variable):
    def calculate(self):
        self.rawarray = np.frombuffer(knownVars[self.var].awkarray.layout.offsets, dtype=np.int64)
        self.rawarray = self.rawarray
    def dependencies(self):
        return [self.var]
        

class ResumJetMass(Variable):
    from_file = False
    def __init__(self, name,  **args):
        self.cstType = 'ufo' if name.startswith('ufo') else 'pflow'
        name = 'ufojet_rmass'
        super().__init__(name,**args)
        
    def calculate(self):
        from ResumConstits import resumJetConstit, mass
        cst = self.cstType
        njet = knownVars[f'{cst}jet_E'].rawarray.size
        p4=resumJetConstit(np.arange(njet),cst)
        self.rawarray = mass(p4)
        np.nan_to_num(self.rawarray,copy=False)
        self.p4 = p4
    def dependencies(self):
        return [f'{self.cstType}constit_'+v for v in ["E","phi","eta",] ]+[f'{self.cstType}jet_ConstitIndex']


def deltaPhi(jet_phi, jet_true_phi, out):
    dphi = jet_true_phi-jet_phi
    tau = np.pi*2
    dphi   = np.where(dphi>np.pi, dphi-tau, dphi)
    out[:] = np.where(dphi<-np.pi, dphi+tau, dphi)
    
knownVarList = [
    JVariable( "ak10ufojet_E" ,     "ak10ufojet_E",    hrange=(0,5e6)),
    JVariable( "ak10ufojet_eta" ,   "ak10ufojet_eta",  hrange=(-1.2,1.2)),
    JVariable( "ak10ufojet_phi" ,   "ak10ufojet_phi",  hrange=(-2,2)),
    JVariable( "ak10ufojet_mass" , "ak10ufojet_mass", hrange=(0,500000)),
    JVariable( "ak10ufojet_pt", "ak10ufojet_pt", hrange=(0,5e6)),
    JVariable( "ak10ufojet_isolated", "ak10ufojet_isolated", hrange=(0,3), src_type=np.int32),
    JVariable( "ak10ufojet_truthMatch", "ak10ufojet_truthMatch", hrange=(0,3), src_type=np.int32), 
    JVariable( "ak10ufojet_isoPFlow", "ak10ufojet_isoPFlow", hrange=(0,3)),
    JVariable( "ak10ufojet_neutralFrac", hrange=(0,1)),
    JVariable( "ak10ufojet_chargedFrac", hrange=(0,1)),
    JVariable( "ak10ufojet_rapidity", "ak10ufojet_rapidity",hrange=(-1.2,1.2)),

    JVariable( "ufojet_E",    hrange=(0,5e6)),
    JVariable( "ufojet_eta",  hrange=(-1.2,1.2)),
    JVariable( "ufojet_phi",  hrange=(-2,2)),
    JVariable( "ufojet_mass", hrange=(0,500000)),
    JVariable( "ufojet_pt", hrange=(0,5e6)),
    JVariable( "ufojet_isolated", hrange=(0,3), src_type=np.int32),
    JVariable( "ufojet_truthMatch", hrange=(0,3), src_type=np.int32),
    JVariable( "ufojet_isoPFlow", hrange=(0,3)),
    JVariable( "ufojet_neutralFrac", hrange=(0,1)),
    JVariable( "ufojet_chargedFrac", hrange=(0,1)),
    JVariable( "ufojet_rapidity",hrange=(-1.2,1.2)),


    JVariable( "ak10pflowjet_E" ,     "ak10pflowjet_E",    hrange=(0,5e6)),
    JVariable( "ak10pflowjet_eta" ,   "ak10pflowjet_eta",  hrange=(-1.2,1.2)),
    JVariable( "ak10pflowjet_phi" ,   "ak10pflowjet_phi",  hrange=(-2,2)),
    JVariable( "ak10pflowjet_mass" , "ak10pflowjet_mass", hrange=(0,500000)),
    JVariable( "ak10pflowjet_pt", "ak10pflowjet_pt", hrange=(0,5e6)),
    JVariable( "ak10pflowjet_isolated", "ak10pflowjet_isolated", hrange=(0,3), src_type=np.int32),
    JVariable( "ak10pflowjet_truthMatch", "ak10pflowjet_truthMatch", hrange=(0,3), src_type=np.int32),
    JVariable( "ak10pflowjet_neutralFrac", hrange=(0,1)),
    JVariable( "ak10pflowjet_chargedFrac", hrange=(0,1)),
    JVariable( "ak10pflowjet_rapidity", "ak10pflowjet_rapidity", hrange=(-1.2,1.2)),

    JVariable( "pflowjet_E",    hrange=(0,5e6)),
    JVariable( "pflowjet_eta",  hrange=(-1.2,1.2)),
    JVariable( "pflowjet_phi",  hrange=(-2,2)),
    JVariable( "pflowjet_mass", hrange=(0,500000)),
    JVariable( "pflowjet_pt", hrange=(0,5e6)),
    JVariable( "pflowjet_isolated", hrange=(0,3), src_type=np.int32),
    JVariable( "pflowjet_truthMatch", hrange=(0,3), src_type=np.int32),
    JVariable( "pflowjet_neutralFrac", hrange=(0,1)),
    JVariable( "pflowjet_chargedFrac", hrange=(0,1)),
    JVariable( "pflowjet_rapidity", hrange=(-1.2,1.2)),


    JetNConstit("ufojet_nconstit", constitIndex='ufojet_ConstitIndex', hrange=(0,50),category='jet' ),
    JetNConstit("pflowjet_nconstit", constitIndex='pflowjet_ConstitIndex', hrange=(0,50), category='jet'),   

    JVariable( "ufojet_ConstitIndex", hrange=(0,4e2), src_type=np.int64),
    JVariable( "pflowjet_ConstitIndex", hrange=(0,2e3), src_type=np.int64),

    CalculatedV("ak10ufojet_deltaEta", vars=['ak10ufojet_eta','ak10truejet_eta'],transfo='ak10truejet_eta-ak10ufojet_eta', hrange=(-0.1,0.1), category='jet' ),
    CalculatedV("ak10ufojet_deltaPhi", vars=['ak10ufojet_phi','ak10truejet_phi'],transfo='ak10truejet_phi-ak10ufojet_phi', hrange=(-0.1,0.1), category='jet' ),
    CalculatedV("ak10ufojet_deltaRap", vars=['ak10ufojet_rapidity','ak10truejet_rapidity'], transfo='ak10truejet_rapidity-ak10ufojet_rapidity', hrange=(-0.1,0.1), category='jet' ),

    CalculatedV("ufojet_deltaEta", vars=['ufojet_eta','truejet_eta'],transfo='truejet_eta-ufojet_eta', hrange=(-0.1,0.1), category='jet' ),
    CalculatedV("ufojet_deltaPhi", vars=['ufojet_phi','truejet_phi'],transfo='truejet_phi-ufojet_phi', hrange=(-0.1,0.1), category='jet' ),
    CalculatedV("ufojet_deltaRap", vars=['ufojet_rapidity','truejet_rapidity'], transfo='truejet_rapidity-ufojet_rapidity', hrange=(-0.1,0.1), category='jet'),

   
    CalculatedV("ak10pflowjet_deltaEta", vars=['ak10pflowjet_eta','ak10truejet_eta'],transfo='ak10truejet_eta-ak10pflowjet_eta', hrange=(-0.1,0.1), category='jet' ),
    CalculatedV("ak10pflowjet_deltaPhi", vars=['ak10pflowjet_phi','ak10truejet_phi'],transfo='ak10truejet_phi-ak10pflowjet_phi', hrange=(-0.1,0.1), category='jet' ),
    CalculatedV("ak10pflowjet_deltaRap", vars=['ak10pflowjet_rapidity','ak10truejet_rapidity'], transfo='ak10truejet_rapidity-ak10pflowjet_rapidity', hrange=(-0.1,0.1), category='jet' ),

    CalculatedV("pflowjet_deltaEta", vars=['pflowjet_eta','truejet_eta'],transfo='truejet_eta-pflowjet_eta', hrange=(-0.1,0.1), category='jet' ),
    CalculatedV("pflowjet_deltaPhi", vars=['pflowjet_phi','truejet_phi'],transfo='truejet_phi-pflowjet_phi', hrange=(-0.1,0.1), category='jet' ),
    CalculatedV("pflowjet_deltaRap", vars=['pflowjet_rapidity','truejet_rapidity'], transfo='truejet_rapidity-pflowjet_rapidity', hrange=(-0.1,0.1), category='jet' ),


    CalculatedV("ufojet_combinedFrac", vars=['ufojet_chargedFrac','ufojet_neutralFrac'], transfo = '1 - ufojet_chargedFrac - ufojet_neutralFrac', hrange=(0,1), category='jet' ),

    JVariable( "ak10truejet_E", hrange=(0,5e6)),
    JVariable( "ak10truejet_mass", hrange=(0,.5e6)),
    JVariable( "ak10truejet_eta", hrange=(-1.2,1.2)),   
    JVariable( "ak10truejet_phi", hrange=(-3.1,3.1)),
    JVariable( "ak10truejet_pt", hrange=(0,5e6)), 
    JVariable( "ak10truejet_rapidity", hrange=(-1.2,1.2)),
 

    JVariable( "truejet_E", hrange=(0,5e6)),
    JVariable( "truejet_mass", hrange=(0,.5e6)),
    JVariable( "truejet_eta", hrange=(-1.2,1.2)),
    JVariable( "truejet_phi", hrange=(-3.1,3.1)),
    JVariable( "truejet_pt", hrange=(0,5e6)),   
    JVariable( "truejet_rapidity", hrange=(-1.2,1.2)),



    CVariable( "ufoconstit_E", hrange=(0,5e5)),
    CVariable( "ufoconstit_eta", hrange=(-1.2,1.2)),
    CVariable( "ufoconstit_pt", hrange=(0,5e5)),
    CVariable( "ufoconstit_mass", hrange=(0,5e5)),
    CVariable( "ufoconstit_phi", hrange=(-2,2)),
    CVariable( "ufoconstit_nCluster", hrange=(0,30), src_type=np.int32),
    ConstitType( "ufoconstit_type", hrange=(0,3), src_type=np.int32),
    CVariable( "ufoconstit_CombinedDeltaEta", hrange=(-1,1)),
    CVariable( "ufoconstit_CombinedDeltaPhi", hrange=(-1,1)),

    CalculatedV("ufoconstit_ClusterEta", vars=['ufoconstit_CombinedDeltaEta','ufoconstit_eta'], transfo = 'ufoconstit_CombinedDeltaEta + ufoconstit_eta', hrange=(-3,3), category='constit'),

    
    
    CVariable( "pflowconstit_E", hrange=(0,5e5)),
    CVariable( "pflowconstit_eta", hrange=(-1.2,1.2)),
    CVariable( "pflowconstit_pt", hrange=(0,5e5)),
    CVariable( "pflowconstit_mass", hrange=(0,5e5)),
    CVariable( "pflowconstit_phi", hrange=(-2,2)),
    CVariable( "pflowconstit_nCluster", hrange=(0,30), src_type=np.int32),
    ConstitType( "pflowconstit_type", hrange=(0,2), src_type=np.int32),

    OffsetsFromAwkArray( "evt_ufoconstitOffset", var="ufoconstit_pt", category='event'),
    	
    RatioVariable("ak10ufojet_r_e", numer = "ak10ufojet_E", denom='ak10truejet_E', hrange=(0.5,2) ,category='jet'),
    RatioVariable("ak10pflowjet_r_e", numer = "ak10pflowjet_E", denom='ak10truejet_E', hrange=(0.5,2) ,category='jet'),
  
    RatioVariable("ufojet_r_e", numer = "ufojet_E", denom='truejet_E', hrange=(0.5,2) ,category='jet'),
    RatioVariable("pflowjet_r_e", numer = "pflowjet_E", denom='truejet_E', hrange=(0.5,2) ,category='jet'),

    RatioVariable("ak10ufojet_r_m", numer = "ak10ufojet_mass", denom='ak10truejet_mass', hrange=(0.5,2) ,category='jet'),
    RatioVariable("ak10pflowjet_r_m", numer = "ak10pflowjet_mass", denom='ak10truejet_mass', hrange=(0.5,2) ,category='jet'),

    RatioVariable("ufojet_r_m", numer = "ufojet_mass", denom='truejet_mass', hrange=(0.5,2) ,category='jet'),
    RatioVariable("pflowjet_r_m", numer = "pflowjet_mass", denom='truejet_mass', hrange=(0.5,2) ,category='jet'),   


    EvtVariable("NPV_event",hrange=(0,50) ),
    EvtVariable("rho", hrange=(0,40)),
    ResumJetMass("ufojet_rmass"),
    RatioVariable("ufojet_r_rm", numer = "ufojet_rmass", denom='truejet_mass', hrange=(0.5,2) ,category='jet'),
]


knownVars = ConfigDict( "vars", **dict( (v.name, v) for v in knownVarList ) )
