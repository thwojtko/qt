from HistoAnalysis import BinnedArraysGraphics as bag 
import numpy as np
import matplotlib.pyplot as plt
plt.ion()


# Create a GraphicSession object to hold all the histo/graph containers
gs = bag.GraphicSession(
    bpsTag ='pTeta',
    plotDir = "plots/",

) # 
from HistoAnalysis.BinnedArrays import BinnedHistos
exec(open('histoscripts/responseAnalysis.py').read())



# ********************************
# style functions
# They return a dictionnary of style (color, labels, line styles,etc...) according to the BinnedArrays they receive.
# They are called within bag.readBAFromFile() for each read BinnedArray
def atlasStyle(ba):
    
    if 'UFO_005' in ba.name:
        return dict(color='green', label='top, small-R UFO jet, DeltaR=0.05')

    if 'UFO' in ba.name:
        return dict(color='blue', label='top, small-R UFO jet')

    if 'PFlow' in ba.name: 
        return dict(color='red', label='top, small-R PFlow jet')
    
    
    


