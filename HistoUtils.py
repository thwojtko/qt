import numpy as np
import numexpr as ne


def average(x, w):
    wsum = w.sum()
    if wsum==0.:
        return 0.
    return (w*x).sum()/wsum

def averageAnderr(x,w,w2):
    wsum = w.sum()
    if wsum==0.:
        return 0, 1.
    over_neff =  w2.sum() / (wsum*wsum)
    m = (w*x).sum()/wsum
    err = np.sqrt( ( (x*x*w).sum()/wsum -m*m )*over_neff)

    return m ,err


import numba
@numba.njit(parallel=True)
def binIndexAndValidtyGeneric(bins, x, indices, validity):
    """Calculate the bin index of 'x' values corresponding to bins defined by 'bins'
    fill these in 'indices' and wether they are valid in 'validity' (array of bool)
    """
    indices[:] = np.searchsorted(bins,x, 'right')

    n = bins.size-1
    for i in numba.prange(x.size):
        indices[i] -= 1 
        validity[i]= (indices[i]>-1)and(indices[i]<n)
        

@numba.njit(parallel=True)
def binIndexAndValidityReg(nbin, xmin, xmax, x, indices, validity):
    
    s = nbin / (xmax - xmin)
    for i in numba.prange(x.size):
        u=int(np.floor( (x[i]-xmin)*s ))
        indices[i]= u
        validity[i]= (u>-1)and(u<nbin)
        

@numba.njit(parallel=True)
def binIndexReg(nbin, xmin, xmax,x, indices):
    """Return the indices of bins in which x fall, assuming there are  'nbin' bins between  xmin and xmax AND overflow and underflow bins.
    Underflows are indexed at 0, overflows indexed at nbin+1. 
    Valid bins are thus in [1, nbin]
    """
    s = nbin / (xmax - xmin)
    for i in numba.prange(x.size):
        # perform (x - xmin)*s +1
        u = numba.int64( (x[i]-xmin)*s+1)
        # clip to 0 and nbins
        u = u if u>0 else 0
        indices[i]= u if u <(nbin+1) else nbin+1
    return indices



@numba.generated_jit(nopython=True)
def addat(target, index, tobeadded):
    """Equivalent of np.add.at(target, index, tobeadded) but much faster. """
    def addat_scalar(target, index, tobeadded):
        for i in range( index.size):        
            target[index[i]] += tobeadded
    def addat_vector(target, index, tobeadded):
        for i in range( index.size):        
            target[index[i]] += tobeadded[i]
    if isinstance(tobeadded, numba.types.Number):
        return addat_scalar
    else:
        return addat_vector



class AxisSpec:
    """Represents 1 dimension in a binned phase space. 
    This is essentially a list of bins on a 1D line which can be defined either by 
      * nbins , (xmin, xmax)  --> regular bins 
      * the list of nbins+1 edges of the bins. 
    In both cases we have : edges[0]=xmin and edges[nbins]=xmax 

    2 ways of indexing are supported when retrieving bin indices :
     - 'rawbin' : indices are simply in [0, nbins-1] and can directly refer to an entry in a numpy array
     - 'histobin' : indices are in [1,nbins] allowing client to use 0/nbins+1 to index under/overflow bins in a histogram (as in ROOT::TH1).
    methods dealing with bins are prefixed either by rawbin either histobin according to the convention they follow.

    """
    isGeV = True
    geVFactor = 1.

    save_att = ['edges', 'isRegular','name', 'title']

    base =None
    
    def __init__(self, name, nbins=None, range=None, edges=None, title=None,  **args):
        self.name = name
        self.title = title or name

        
        if edges is not None:
            edges = np.array(edges)
            d0 = (edges[1]-edges[0])
            self.isRegular = np.all( np.abs(edges[1:]-edges[:-1] -d0) < d0*1e-3  )
            if nbins or range:
                print("AxisSpec ERROR specifiy either edge eithe nbins and range")
                raise
            self.edges = edges
            self.nbins = len(edges)-1
            self.range = edges[0], edges[-1]
        elif range is not None:
            self.isRegular = True
            if edges:
                print("AxisSpec ERROR specifiy either edge eithe nbins and range")
                raise
            self.nbins = nbins
            self.range = range
            self.edges = np.linspace(range[0], range[1],nbins+1)
            
        
        for k,v in args.items():
            setattr(self,k,v)

    def rawbinIndexAndValidity(self,x, indices=None, validity=None):
        """Returns (indices, validity) where 
            - indices contain indices of bins to which x values belong
            - validity is True or False depending if x is in the range or not,
        IMPORTANT 'rawbin' convention : returned valid index are in [0,nbins-1] (see above)
        """

        if validity is None:
            validity = np.empty(x.size, dtype=np.bool)
        if indices is None:
            indices = np.empty(x.size,dtype=np.int64)
        
        if self.isRegular:
            binIndexAndValidityReg(self.nbins, self.range[0],self.range[1], x, indices, validity)
        else:
            binIndexAndValidtyGeneric(self.edges, x, indices, validity)
        return indices,validity

    def histobinIndex(self, x, indices=None):
        """Returns the indices of bins to which x values belong.
        IMPORTANT, 'histobin' convention : valid index are in [1,nbins] (see above)
        """
        if self.isRegular:
            if indices is None:
                indices = np.zeros(x.size,dtype=np.int64)            
            return binIndexReg(self.nbins, self.range[0], self.range[1], x, indices)
        else:
            if indices:
                indices[:] = np.searchsorted(self.edges,x, 'right')
                return indices
            else:
                return np.searchsorted(self.edges,x, 'right')
            

    def histobinCenter(self, i):
        """IMPORTANT 'histobin' convention : i is supposed to be in [1,nbins]"""
        return 0.5*(self.edges[i-1]+self.edges[i])
            
    def rawbinCenters(self,i=None):
        if i is None:
            return (self.edges[:-1]+self.edges[1:])*0.5
        else:
            return (self.edges[i+1]+self.edges[i])*0.5

    def describeBin(self, i, j=None):
        """IMPORTANT 'rawbin' convention """
        low = self.edges[i]
        up = self.edges[(j or i)+1]
        #low , up = self.edges[i:i+2]
        if self.isGeV:
            low = int(low*self.geVFactor)
            up  = int(up*self.geVFactor)
            binDesc='{} $\in [{:d}, {:d}]$' # self.geVDesc
        else:
            binDesc='{} $\in [{:.2f}, {:.2f}]$'
        return binDesc.format(self.title , low, up)

    def histobinWidth(self, i=1):
        return self.edges[i] -self.edges[i-1] 

    def rawbinWidth(self, i=0):
        return self.edges[i+1] -self.edges[i] 

    def dump(self):
        print( self.name, self.range, self.edges)


    def saveTo(self, out, prefix=None):
        prefix= prefix or self.name+"_"
        for att in self.save_att:
            out[prefix+att] = getattr(self, att)
    
    def loadFrom(self, inp, prefix=None):
        prefix= prefix if prefix is not None else self.name+"_"
        for att in self.save_att:
            setattr(self, att, inp.get(prefix+att,"") )
        e = self.edges
        self.range = (e.min(), e.max() )
        self.nbins = len(e)-1


    def __getitem__(self, s):
        if isinstance(s,int) : s=slice(s,s+1)
        if s.step is not None:
            raise Exception("AxisSpec slicing with stride not supported")

        start, stop, _ = s.indices(self.nbins)
        newa = AxisSpec(self.name, title = self.title)
        newa.isRegular = self.isRegular
        newa.edges = self.edges[start:stop+1]
        newa.range = newa.edges[0], newa.edges[-1]
        newa.nbins = len(newa.edges)-1
        newa.isGeV = self.isGeV
        newa.geVFactor = self.geVFactor
        newa.base = self
        return newa


## Histo manipulation
class HistoBase:
    #save_att = ['bins', 'hrange', 'hcontent', 'xbins']
    save_att = ['hcontent', 'hw2']
    def __init__(self,  hrange=None, x=None, y=None, w=None, name=None, title=None, xaxis=None, yaxis=None,
                 hcontent=None, hw2=None,):
        """ 
        Create a Histogram : 
          - binning : obtained from axis (an AxisSpec) if given. Else uses hrange expected in the form (nbins, (lower_edge, upper_edge)))
          - content : is x (and y, w) is given -> fills the histo with the x values. Else if hcontent is given adopt directly this array as the bin content

        same convention as ROOT : bin at i=0 is underflow, bin at i=nbins+1 is overflow.
        """

        def buildAxis(hrange, name, title):
            if axis is not None:
                return axis
            elif hrange is not None:
                axis = AxisSpec(name,nbins=hrange[0], range=hrange[1],title=title)
            else:
                axis = AxisSpec(name, title=title)
            return axis

        title = title or name+':xaxis:yaxis'
        if hrange is not None:
            titles = title.split(':')
            title=titles[0]
            if xaxis is not None or yaxis is not None:
                raise Exception("HistoBase ERROR : do not specify hrange AND xaxis or yaxis ")
            if len(hrange)==2:
                self.xaxis = AxisSpec('xaxis',nbins=hrange[0], range=hrange[1],title =title)
            elif len(hrange)==4:
                self.xaxis = AxisSpec('xaxis',nbins=hrange[0], range=hrange[2],title=titles[1])
                self.yaxis = AxisSpec('yaxis',nbins=hrange[3], range=hrange[4],title=titles[2])
        elif xaxis is not None:
            self.xaxis = xaxis
            self.yaxis = yaxis
        else:
            self.xaxis = AxisSpec('xaxis')
            
        if x is not None:
            if hcontent is not None:
                raise Exception("HistoBase ERROR : do not specify x AND hcontent ")
            self.fromArrays(x,y=y,w=w)
        elif hcontent is not None:
            self.hcontent = hcontent
            if hw2 is None: hw2 = hcontent
            self.hw2 = hw2
        else:
            self.hcontent = np.zeros(self.xaxis.nbins+2)
            self.hw2 = None
        self.name = name or (self.__class__.__name__)
        self.title = title or self.name
        

    def asDict(self):
        d = dict( (self.name+'_'+att, getattr(self, att) ) for att in self.save_att)
        self.xaxis.saveTo(d, self.name+"_x") 
        return d
    
    def save(self, fname):
        d = self.asDict()
        np.savez(fname, **d)

    def load(self, fname):
        f = np.load(fname)
        for att in self.save_att:
            setattr(self, att, f[self.name+'_'+att] )
        self.xaxis.loadFrom( f, self.name+"_x")



class Histo1D(HistoBase):

    def fill(self, x, w=None):
        x_i = self.xaxis.histobinIndex(x)
        addat(self.hcontent, x_i, 1. if w is None else w)

        # now deal with weights 
        if w is not None:
            if self.hw2 is None:
                self.hw2 = np.zeros(self.xaxis.nbins+2)
            #self.hw2 += np.bincount(x_i, w*w , self.xaxis.nbins+2)
            addat(self.hw2, x_i, w*w)
        else:
            if self.hw2 is None:
                self.hw2 = self.hcontent
            if self.hw2 is not self.hcontent:
                addat(self.hw2, x_i, 1.)
                #self.hw2 += wcount
        

    def fromArrays(self, x,   y=None,w=None):
        if x is None: return
        x_i = self.xaxis.histobinIndex(x)
        w1 = w if w is not None else np.ones_like(x)

        self.hcontent = np.bincount(x_i, w1 , self.xaxis.nbins+2)
        if w is not None:
            self.hw2 = np.bincount(x_i, w*w , self.xaxis.nbins+2)
        else:
            self.hw2 = self.hcontent
        return self.hcontent
        
    def findBin(self,x):
        return self.axis.histobinIndex(x)
        
    def binContentAt(self, x,  outC=None , _x_i=None, scaleOutC=False):
        x_i = self.xaxis.histobinIndex(x)
        if outC is None:
            outC=self.hcontent[x_i]
        else:
            if scaleOutC: outC *=self.hcontent[x_i]
            else: outC[:] = self.hcontent[x_i]
        return outC

    def binWidth(self, i=0):
        return self.xaxis.histobinWidth(i)
    
    def mean(self):
        ed = self.xaxis.edges
        c = 0.5*(ed[:-1]+ed[1:])
        return average( c, self.hcontent[1:-1])

    def meanAnderr(self):
        w = self.hcontent[1:-1]
        w2 = self.hw[1:-1]
        ed = self.edges
        c = 0.5*(ed[:-1]+ed[1:])
        return averageAnderr(c, w, w2) 

    def nEffEntries(self):
        n = self.hw2[1:-1].sum()
        return 0 if n==0. else self.hcontent[1:-1].sum()**2/n

    def normalize(self):
        s=self.hcontent.sum()
        if s>0:
            self.hcontent/=s
            self.hw2/=s*s
    
    def draw(self, ax=None, same=False, ds='steps-post', err=False,  **opts):
        from matplotlib import pyplot as plt
        if ax is None: ax=plt.gca()
        if not same: ax.cla()
        #rr= ax.hist(axis._fXbins[:-1], axis._fXbins , weights=self.hcontent[1:-1], **opts)
        opts['ds']=ds
        opts.setdefault('label', self.title)
        c = self.xaxis.edges[:-1]
        if err:
            rr= ax.errorbar(c, self.hcontent[1:-1], np.sqrt(self.hw2[1:-1]), **opts )
        else:
            rr = ax.plot(c, self.hcontent[1:-1],  **opts )
        #print("------> draw", repr(ax), same)
        return rr[0]

    
class Histo2D(HistoBase):
    save_att = HistoBase.save_att+[ 'ybins']        

    def asDict(self):
        d = dict( (self.name+'_'+att, getattr(self, att) ) for att in self.save_att)
        self.xaxis.saveTo(d, self.name+"_x") 
        self.yaxis.saveTo(d, self.name+"_y") 
        return d

    def load(self, fname):
        f = np.load(fname)
        for att in self.save_att:
            setattr(self, att, f[self.name+'_'+att] )
        self.xaxis.loadFrom( f, self.name+"_x")
        self.yaxis.loadFrom( f, self.name+"_y")

    
    def fromArrays(self, x, y, bins=None, hrange=None, w=None):
        pass

    def findBin(self,x,y):
        pass
        
    def binContentAt(self, x, y, outC=None , _x_i=None, _y_i=None):
        pass
    
    def draw(self,ax=None,norm=None, **opts):
        from matplotlib import pyplot as plt
        if ax is None: ax=plt.gca()
        if hasattr(ax, 'colorbar'):
            ax.colorbar.remove()
            del(ax.colorbar)
        
        m=ax.pcolormesh(self.xaxis.edges,self.yaxis.edges,self.hcontent[1:-1,1:-1].T,norm=norm, cmap=opts.pop('cmap','Reds'))
        ax.set_xlabel(self.xaxis.title)
        ax.set_ylabel(self.yaxis.title)
        ax.set(**opts)
        cb=plt.colorbar(m, ax=ax)
        ax.colorbar = cb
        
