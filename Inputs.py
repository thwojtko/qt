import awkward as ak
import uproot
from Variables import Variable, knownVars
import numpy as np

class Inputs:
    """Collect input files and provide utility to 
     - iterate over files
     - load arrays corresponding to list of Variable (and connecting the Variable object to these arrays)
    """
    def __init__(self, config):
        
        from glob import glob
        
        self.fileList = glob( config.inputDir+'/'+config.filePattern )
        print('Input file list = ',self.fileList)
        self.treename = config.treename
        self.fileTreeList = [f+':'+config.treename for f in self.fileList]
        self.setVarList( config.varList )

        self.config = config

        
    def setVarList(self, varList):
        """Sets the list of variable to be automatically loaded by the loading functions """
        self.varList  = [ knownVars[v] if isinstance(v,str) else v for v in varList ]
        deps = {}
        for v in self.varList:
            deps.update( (d,None) for d in v.dependencies())
        self.varList += [ knownVars[v] for v in deps.keys() ]
        
        

        self.tree = None # no tree opened yet



    # *********************************************
    # load data 
    # *********************************************
    def iterate(self,step_size=None):
        """call uproot.iterate on our files to load our variables """
        step_size = step_size or self.config.step_size
        vnames = [v.src_name for v in self.varList if v.from_file]
        print("uuuuuuuu", self.fileTreeList, vnames)
        for a in uproot.iterate(self.fileTreeList,vnames, step_size=step_size):
            self.connectVars(a)
            yield a


    def concatenate(self):
        """call uproot.concatenate on our files to load our variables """
        vnames = [v.src_name for v in self.varList if v.from_file]
        a=uproot.concatenate(self.fileTreeList,vnames,num_workers=2)
        self.connectVars(a)
        return a
            
    def loadFile(self,i):
        """call uproot.tree.arays load our variables from file i  """
        self.tree = uproot.open(self.fileTreeList[i])
        vnames = [v.src_name for v in self.varList if v.from_file]
        a = self.tree.arrays( vnames )
        self.connectVars(a)
        return a

        
    def connectVars(self, arrayCont):
        """Point our Variable.awkarray/rawarray to the arrays within the array container arrayCont """
        for v in self.varList:
            if v.from_file:
                v.awkarray = arrayCont[v.src_name]
                v.buildRawArray()
                #if v.awkarray.ndim>1:
                #    v.rawarray  = ak.flatten(v.awkarray).to_numpy()
                #else:
                #    v.rawarray  = ak.flatten(v.awkarray,0).to_numpy()
        for v in self.varList:
            v.calculate()


    def readTree(self,i=0):
        self.tree = uproot.open(self.fileTreeList[i])
        return self.tree
                
        
            
    def sortFiles(self):
        sizes = [uproot.open(f).num_entries for f in self.fileTreeList]
        self.fileList = [ self.fileList[i] for i in np.argsort(sizes)[::-1] ]
        self.fileTreeList = [f+':'+self.treename for f in self.fileList]
